package org.jeecgframework.poi.excel.entity.enmus;

/**
 * Excel Type
 * @author JueYue
 * @date 2014年12月29日 下午9:08:21
 */
public enum ExcelType {
    
    HSSF, XSSF;

}
